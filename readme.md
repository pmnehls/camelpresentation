
****** JMS, ACTIVEMQ AND CAMEL PRESENTATION *******


***ContentBasedRouterDemo.java***
     Takes messages located in data/TedData, sends them to ActiveMQ, and routes them to different folders based on the message content.  Messages are found in output folder.

***MulticastDemo.java***
     Similar to Content Based Router, except routing is done in parallel.  Messages are found in output folder.

***FilterDemo.java***
    Takes messages located in data/TedData, sends them to ActiveMQ, and filters messages out based on a predicate, and routes them to different folders.  Messages are found in output folder.

***Translator.java***
    Translator takes in a text file in l33tspeak and turns it into regular text

***Splitter.java***
    Splitter takes in a text file and splits it at a line. It places the items in a queue to be consumed by Aggregator

***Aggregator.java***
    Aggregator re-assembles the messages split by splitter.

***RealWorldExample***

RealWorldExample utilizes several different features of Camel (along with ActiveMQ and JMS) to create an order handling system for a Pet Shop.
OrderProducer takes XML and txt files as order inputs, and turns them into order objects, passing them to an Order Queue.
OrderConsumer removes these order objects from the queue, then processes them and routes the item details within the order to
the correct folder in output, based on the department of the item.

To run the application:
Run OrderProducer (takes about 10 seconds)
Run OrderConsumer


RUNNING A CAMEL ActiveMQ Project
================================

This project embeds Apache ActiveMQ together with Apache Camel.

To build this project use

    mvn install

To run this project use the following Maven goal

    mvn camel:run

For more help see the Apache Camel documentation

    http://camel.apache.org/