package edu.uchicago.CamelGroup.realWorldExample;

import java.util.ArrayList;
import java.util.List;

/**
 * PetStore is a thread safe singleton, since we are creating an order
 * management interface for a single pet store
 */
class PetStore {

    private static PetStore petStore = null;
    private static final Object mutex = new Object();

    private String name;
    private static List<Item> items = new ArrayList<>();

    static int currItemNumber = 0;

    enum Department {
        DOG_STUFF, CAT_STUFF, EXOTIC_ANIMALS, FISH, DO_NOT_CARRY, OUT_OF_STOCK
    }

    private PetStore(){
        initInventory();
    }

    static PetStore getInstance(){
        if (petStore == null){

            synchronized (mutex){

                if (petStore == null) {
                    petStore = new PetStore();
                }
            }
        }

        return petStore;
    }

    void setPetStoreName(String n){
        name = n;
    }

    private static void initInventory(){

        Item i1 = new Item("Regular Dog Food - 20lb", 15.00, 50, Department.DOG_STUFF);
        Item i2 = new Item("Fancy Dog Food - 15lb", 39.95, 10, Department.DOG_STUFF);
        Item i3 = new Item("Cat nip - 6oz", 3.50, 20, Department.CAT_STUFF);
        Item i4 = new Item("African Grey Parrot", 1999.99, 2, Department.EXOTIC_ANIMALS);
        Item i5 = new Item("Dog Leash", 4.50, 25, Department.DOG_STUFF);
        Item i6 = new Item("Cat Litter - 15 lb", 12.99, 30, Department.CAT_STUFF);
        Item i7 = new Item("Goldfish", 0.50, 200, Department.FISH);

        items.add(i1);
        items.add(i2);
        items.add(i3);
        items.add(i4);
        items.add(i5);
        items.add(i6);
        items.add(i7);
    }

    /*if in stock, return 1, if out of stock, return 0, if not an item
    the store carries, return -1 */
    int isInStock(Item i){

        for (Item item : items){
            if (i.getName().equals(item.getName())){
                if (item.getStock() > 0){
                    return 1;
                }
                else {
                    return 0;
                }
            }
        }

        return -1;
    }

    Department getDepartment(Item i){
        for (Item item : items){
            if (i.getName().equals(item.getName())){
                if (item.getStock() > 0){
                    return item.getDepartment();
                }
                else {
                    return Department.OUT_OF_STOCK;
                }
            }
        }

        return Department.DO_NOT_CARRY;
    }

    String getName(){
        return name;
    }
}
