package edu.uchicago.CamelGroup.realWorldExample;

/**
 * This class will pull orders out of the Order queue and process them, routing
 * them to the appropriate location
 */

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

class OrderConsumer {

    //singleton instance of the pet store, used to look up items in inventory
    private static PetStore myPetStore = PetStore.getInstance();

    //splits orders into pending items, so we can pass the items to correct location
    private static class OrderSplitter implements Processor{

        @Override
        public void process(Exchange exchange) throws Exception {

            List<PendingItem> pendingItems = new ArrayList<>();

            Order o = (Order) exchange.getIn().getBody();

            for (Item i : o.getItems()){

                int isInStock = myPetStore.isInStock(i);

                switch (isInStock){
                    case 1:
                        PendingItem pending = new PendingItem(o.getOrderID(),o.getCostumerName(),
                                i, myPetStore.getDepartment(i));
                        pendingItems.add(pending);
                        break;
                    case 0:
                        PendingItem pending2 = new PendingItem(o.getOrderID(),o.getCostumerName(),
                                i, PetStore.Department.OUT_OF_STOCK);
                        pendingItems.add(pending2);
                        break;
                    case -1:
                        PendingItem pending3 = new PendingItem(o.getOrderID(),o.getCostumerName(),
                                i, PetStore.Department.DO_NOT_CARRY);
                        pendingItems.add(pending3);
                        break;
                }

            }

            exchange.getIn().setBody(pendingItems);

        }
    }

    /*sets the department in the header and creates the text based message which is our final output
     for the purposes of this demo */
    private static class ItemProcessor implements Processor {

        @Override
        public void process(Exchange exchange) throws Exception {

            Random ran = new Random();

            PendingItem pending = (PendingItem) exchange.getIn().getBody();
            exchange.getIn().setHeader("department", pending.getDepartment());

            String newBody = "***** " + myPetStore.getName() + " *****" +"\n\n";
            newBody += "Customer Name: " + pending.getCustomerName() + "\n";
            newBody += "Order Number: " + pending.getOrderNumber() + "\n";
            newBody += "Item ordered: " + pending.getItem().getName() + "\n";

            exchange.getIn().setBody(newBody);

            String newFileName = new SimpleDateFormat("yyyy.MM.dd").format(new Date())
                    + "-" + pending.getCustomerName() + "-"
                    + ran.nextInt(99999) + ".txt";
            exchange.getIn().setHeader("CamelFileName", newFileName);

        }
    }

    public static void main(String args[]) throws Exception {

        //name our pet store
        myPetStore.setPetStoreName("Waggin' Tails");

        //create an instance of each processor
        Processor orderSplitter = new OrderSplitter();
        Processor itemProcessor = new ItemProcessor();

        // create CamelContext
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("tcp://localhost:61616");

        //this allows us to pass serialized, user created classes
        connectionFactory.setTrustAllPackages(true);

        context.addComponent("jms",
                JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));

        // add our route to the CamelContext
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("jms:queue:INCOMING_ORDERS")
                        .log("Processing order: ${header.CamelFileName}")
                        .convertBodyTo(Order.class)
                        .process(orderSplitter)
                        .split(body())
                        .process(itemProcessor)
                        .choice()
                            .when(header("department").isEqualTo(PetStore.Department.DOG_STUFF))
                                .to("file:output/petStore/Dog_Stuff?fileName=${header.CamelFileName}")
                            .when(header("department").isEqualTo(PetStore.Department.CAT_STUFF))
                                .to("file:output/petStore/Cat_Stuff?fileName=${header.CamelFileName}")
                            .when(header("department").isEqualTo(PetStore.Department.EXOTIC_ANIMALS))
                                .to("file:output/petStore/Exotic_Animals?fileName=${header.CamelFileName}")
                            .when(header("department").isEqualTo(PetStore.Department.FISH))
                                .to("file:output/petStore/Fish?fileName=${header.CamelFileName}")
                            .when(header("department").isEqualTo(PetStore.Department.OUT_OF_STOCK))
                                .to("file:output/petStore/Out_Of_Stock?fileName=${header.CamelFileName}")
                            .when(header("department").isEqualTo(PetStore.Department.DO_NOT_CARRY))
                                .to("file:output/petStore/Requested_Items?fileName=${header.CamelFileName}");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        // start the route and let it do its work
        context.start();
        Thread.sleep(10000);

        // stop the CamelContext
        context.stop();
    }
}

