package edu.uchicago.CamelGroup.realWorldExample;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Orders are comprised of items and passed as the body of camel messages to the order queue
 */
class Order implements Serializable{

    private String costumerName;
    private List<Item> items = new ArrayList<>();
    private int orderID;

    Order(String name, List<Item> orderContents){

        costumerName = name;

        items.addAll(orderContents);

        orderID = OrderProducer.currOrderID;
        OrderProducer.currOrderID++;

    }

    String getCostumerName() {
        return costumerName;
    }

    List<Item> getItems() {
        return items;
    }

    int getOrderID(){
        return orderID;
    }



}
