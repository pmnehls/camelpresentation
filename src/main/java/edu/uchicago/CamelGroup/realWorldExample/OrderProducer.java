package edu.uchicago.CamelGroup.realWorldExample;

/**
 * Created by pmnehls on 8/11/16.
 *
 * processes XML and text files and turns them into orders
 */
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class OrderProducer {

    /* takes an a message containing an xml order and turns it into an Order object */
    private static class XMLProcessor implements Processor{


        @Override
        public void process(Exchange exchange) throws Exception {
            List<Item> items = new ArrayList<>();
            int rowCounter = 1;
            String[] body = exchange.getIn().getBody(String.class).split("\n");

            final Pattern nameP = Pattern.compile("<customerName>(.*?)</customerName>",Pattern.DOTALL);
            final Pattern itemP = Pattern.compile("<name>(.*?)</name>");
            final Pattern quantityP = Pattern.compile("<quantity>(.*?)</quantity>");

            Matcher nameM = nameP.matcher(body[rowCounter]);
            String name = "";
            if (nameM.find()) {
                name = nameM.group(1);
            }

            rowCounter = rowCounter + 3;

            while (rowCounter < body.length){
                Matcher itemM = itemP.matcher(body[rowCounter]);
                String itemName = "";
                if (itemM.find()) {
                    itemName = itemM.group(1);
                    rowCounter++;
                }
                Matcher quantityM = quantityP.matcher(body[rowCounter]);
                int qty = 0;
                if (quantityM.find()) {
                    qty = Integer.parseInt(quantityM.group(1));
                }
                Item itemToAdd = new Item(itemName);
                for (int i = 0; i < qty; i++){
                    items.add(itemToAdd);
                }

                rowCounter = rowCounter + 3;

            }

            Order order = new Order(name, items);

            exchange.getIn().setBody(order);

        }

    }

    private static class TXTProcessor implements Processor{

        @Override
        public void process(Exchange exchange) throws Exception {

            List<Item> items = new ArrayList<>();

            String[] body = exchange.getIn().getBody(String.class).split("\n");

            String name = body[0].substring(6);

            for (int i = 1; i <  body.length; i++){
                String itemName = body[i];
                items.add(new Item(itemName));
            }

            Order o = new Order(name, items);


            exchange.getIn().setBody(o);

        }
    }

    static int currOrderID = 0;

    public static void main(String args[]) throws Exception {

        // create CamelContext
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("tcp://localhost:61616");
        context.addComponent("jms",
                JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));

        final Processor xmlProcessor = new XMLProcessor();
        final Processor txtProcessor = new TXTProcessor();

        // add our route to the CamelContext
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:data/orders-xml?noop=true","file:data/orders-txt?noop=true")
                        .log("RECEIVED ${header.CamelFileName}")
                        .choice()
                            .when(simple("${header.CamelFileName} contains '.xml'"))
                                .process(xmlProcessor)
                                //.convertBodyTo(Order.class)
                                .to("jms:queue:INCOMING_ORDERS?jmsMessageType=Object")
                            .when(simple("${header.CamelFileName} contains '.txt'"))
                                .process(txtProcessor)
                                //.convertBodyTo(Order.class)
                                .to("jms:queue:INCOMING_ORDERS?jmsMessageType=Object")
                            .otherwise()
                                .to("jms:queue:INVALID_ORDERS");


                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        // start the route and let it do its work
        context.start();
        Thread.sleep(10000);

        // stop the CamelContext
        context.stop();
    }
}
