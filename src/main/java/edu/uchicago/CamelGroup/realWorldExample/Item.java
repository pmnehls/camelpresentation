package edu.uchicago.CamelGroup.realWorldExample;

import java.io.Serializable;

/**
 *
 */
class Item implements Serializable{

    //price and itemNumber are not used in this demo but are implemented and can be used
    private String name;
    private double price;
    private int stock;
    private int itemNumber;

    private PetStore.Department department;

    Item(String n, double p, int s, PetStore.Department d){
        name = n;
        price = p;
        stock = s;
        itemNumber = PetStore.currItemNumber;
        department = d;
        PetStore.currItemNumber++;
    }

    //constructor for orders, when all we have is the name
    Item(String n){
        name = n;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    int getStock() {
        return stock;
    }

    PetStore.Department getDepartment() {
        return department;
    }

}
