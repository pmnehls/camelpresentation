package edu.uchicago.CamelGroup.realWorldExample;

import java.io.Serializable;

/**
 * Class to handle intermediate state when processing orders
 */
class PendingItem implements Serializable{

    private int orderNumber;
    private String customerName;
    private Item item;
    private PetStore.Department department;

    PendingItem(int ordNum, String customer, Item i, PetStore.Department d){
        this.orderNumber = ordNum;
        this.customerName = customer;
        this.item = i;
        this.department = d;
    }

    int getOrderNumber(){
        return orderNumber;
    }

    String getCustomerName(){
        return customerName;
    }

    Item getItem(){
        return item;
    }

    PetStore.Department getDepartment(){
        return department;
    }
}
