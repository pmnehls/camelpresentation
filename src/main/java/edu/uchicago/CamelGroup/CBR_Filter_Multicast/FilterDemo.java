package edu.uchicago.CamelGroup.CBR_Filter_Multicast;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;

import static org.apache.camel.builder.PredicateBuilder.or;

/**
 * Created by theodoreprekop on 8/5/16.
 */
public class FilterDemo {
    public static void main(String args[]) throws Exception {
        // create CamelContext
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        context.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));


        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:data/TedData?noop=true")
                        //.log("RECEIVED:  jms queue: ${body} from file: ${header.CamelFileNameOnly}")
                        .convertBodyTo(String.class)
                        .choice()
                        .when(body().contains("Florida Gators"))
                        .to("jms:queue:GatorQueue")
                        .when(body().contains("cat"))
                        .to("jms:queue:CatQueue")
                        .otherwise()
                        .to("jms:queue:DefaultQueue");


                //Example of Predicate:  Don't have to do everything in big mess of paraentheses
                Predicate trump = body().contains("Trump");
                Predicate trumpAndSeminole = or(trump, body().contains("Seminoles"));

                //Comment the following three lines out to see the messages in the queue
                from("jms:DefaultQueue").filter(trumpAndSeminole).process(exchange -> System.out.println("Received Default Message: ")).to("file:output/messages/filter/default");




                //the process method takes a Functional Interface Process, so we can replace with a lambda expression
                from("jms:CatQueue").process(exchange -> System.out.println("Score!  Received Cat Message: ")).to("file:output/messages/filter/cats");
                from("jms:GatorQueue").process(exchange -> System.out.println("Nice!  Received Gator Message: ")).to("file:output/messages/filter/gators");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // start the route and let it do its work
        context.start();
        Thread.sleep(10000);

        // stop the CamelContext
        context.stop();

    }
}
