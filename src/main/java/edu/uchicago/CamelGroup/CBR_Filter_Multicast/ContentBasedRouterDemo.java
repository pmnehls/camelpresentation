package edu.uchicago.CamelGroup.CBR_Filter_Multicast;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;

/**
 * Created by theodoreprekop on 8/5/16.
 */
public class ContentBasedRouterDemo {
    public static void main(String args[]) throws Exception {

        // create CamelContext, which is a Top Level container
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

        /**
         * JMS (Java Message Service) is a Java API that allows you to create, send, receive, and
         * read messages. It also mandates that messaging is asynchronous and has specific elements
         * of reliability, like guaranteed and once-and-only-once delivery. JMS is the de
         * facto messaging solution in the Java community.
         *
         */

        //Add an endpoint to route
        context.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));

        //Add a route to the Current camel context.  RouteBuilder is an abstract class;
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:data/TedData?noop=true")
                        //.log("RECEIVED:  jms queue: ${body} from file: ${header.CamelFileNameOnly}")
                        .convertBodyTo(String.class)
                        .choice()
                        .when(simple("${body} contains 'Florida Gators'"))
                            .to("jms:queue:GatorQueue")
                        .when(simple("${body} contains 'cat'"))
                            .to("jms:queue:CatQueue")
                        .otherwise()
                            .to("jms:queue:DefaultQueue");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //Comment the following three lines out to see the messages in the queue

                //Processer is a functional interface, so we can use a Lambada
                from("jms:DefaultQueue").process(exchange -> System.out.println("Received Default Message: ")).to("file:output/messages/default");
                from("jms:CatQueue").process(exchange -> System.out.println("Score!  Received Cat Message: ")).to("file:output/messages/cats");
                from("jms:GatorQueue").process(exchange -> System.out.println("Nice!  Received Gator Message: ")).to("file:output/messages/gators");
            }
        });

        // start the route and let it do its work
        context.start();
        Thread.sleep(10000);

        // stop the CamelContext
        context.stop();

    }
}
