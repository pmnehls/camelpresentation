package edu.uchicago.CamelGroup.CBR_Filter_Multicast;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;

/**
 * Created by theodoreprekop on 8/11/16.
 */
public class MulticastDemo {

    public static void main(String args[]) throws Exception {
        // create CamelContext, which is a Top Level container
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");


        //Add an endpoint to route
        context.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));

        //Add a route to the Current camel context.  RouteBuilder is an abstract class;
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:data/TedData?noop=true")
                        //.log("RECEIVED:  jms queue: ${body} from file: ${header.CamelFileNameOnly}")
                        .convertBodyTo(String.class)
                        .choice()
                        .when(simple("${body} contains 'Florida Gators'"))
                        .to("jms:queue:GatorQueue")
                        .when(simple("${body} contains 'cat'"))
                        .to("jms:queue:CatQueue")
                        .otherwise()
                        .to("jms:queue:DefaultQueue");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //Comment the following three lines out to see the messages in the queue

                //Easy parallel processing.
                from("jms:DefaultQueue").process(exchange -> System.out.println("Received Default Message (Parallel): "))
                        .multicast().parallelProcessing().to("file:output/messages/TedDefault", "file:output/messages/MarkDefault", "file:output/messages/AndrewDefault");
                from("jms:CatQueue").process(exchange -> System.out.println("Score!  Received Cat Message  (Parallel): "))
                        .multicast().parallelProcessing().to("file:output/messages/TedCats", "file:output/messages/MarkCats", "file:output/messages/AndrewCats");
                from("jms:GatorQueue").process(exchange -> System.out.println("Nice!  Received Gator Message (Parallel): "))
                        .multicast().parallelProcessing().to("file:output/messages/TedGators", "file:output/messages/MarkGators", "file:output/messages/AndrewGators");
            }
        });

        // start the route and let it do its work
        context.start();
        Thread.sleep(10000);

        // stop the CamelContext
        context.stop();
    }
}
